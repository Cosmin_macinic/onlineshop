package com.online.shop.entities;

import com.online.shop.enums.UserRole;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    private String fullName;

    private String email;

    private String password;

    private String address;

    private UserRole userRole;

    @OneToOne(mappedBy = "user")
    private ShoppingCart shoppingCart;
}
