package com.online.shop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//  @Autowired
 //   private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/register").permitAll()
                .antMatchers("/register","/css/**","/vendors/**","/img/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/home",false)
                .loginPage("/login")
                .permitAll()
                .failureUrl("/login-error")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login")
                .permitAll();
        http.csrf().disable();
    }


}


 /*
        CONFIFGURARE SPRING SECURITY :
         .authorizeRequests()   -> Orice request il vrem autorizat
         .antMatchers("/registration","/assets/**").permitAll() -> Daca vede in URL ceva care se potriveste cu registration, assets, login , etc ...  va permite accesul tuturor
         .anyRequest().authenticated() -> orice request vrem sa le autentificam
         .and()
         .formLogin() -> adauga formul de login
         .loginPage("/login").permitAll() -> permite accesul tuturor
         .failureUrl("/login-error") -> failure care trimite catre /LoginError (in caz de use/parola gresita, user blocat, etc)
         .and()
         .logout() -> ofera functionalitatea de logout
         .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) -> url de logout
         .logoutSuccessUrl("/login") -> redirect catre login
         .permitAll(); -> permite accesul tuturor
         */