package com.online.shop.controller;

import com.online.shop.dto.ProductDto;
import com.online.shop.dto.UserDto;
import com.online.shop.service.ProductService;
import com.online.shop.service.UserService;
import com.online.shop.validator.ProductValidator;
import com.online.shop.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;

@Controller

public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;


    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private UserValidator userValidator;


    @GetMapping("/addProduct")
    public String addProductPageGet(Model model) {
        //se va executa "business logic" :)
        //dupa care intoarcem un nume de pagina

        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPagePost(@ModelAttribute ProductDto productDto, BindingResult bindingResult, @RequestParam("productImage") MultipartFile multipartFile) throws IOException {

        System.out.println(multipartFile.getBytes());
        productValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }

        productService.addProduct(productDto, multipartFile);
        return "redirect:/addProduct";
    }

    @GetMapping("/home")
    public String homePageGet(Model model) {
        List<ProductDto> productDtoList = productService.getAllProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        System.out.println(productDtoList);
        return "homepage";

    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model) {
        ProductDto productDto = productService.getOneProductDtoById(productId);
        model.addAttribute("selectedProductDto", productDto);
        System.out.println("Am dat click pe produsul cu id-ul " + productId);
        return "viewProduct";
    }

    @GetMapping("/register")
    public String registerPageGet(Model model,@RequestParam(value = "userAddedSuccessfully", required = false)  Boolean userAddedSuccessfully) {
        System.out.println(userAddedSuccessfully);
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
      if (userAddedSuccessfully!= null && userAddedSuccessfully) {
           model.addAttribute("message","User has been added successfully!");
      }
        return "register";
    }

    @PostMapping("/register")
    public String registerPagePost(@ModelAttribute UserDto userDto, BindingResult bindingResult,
                                   RedirectAttributes redirectAttributes) {

        System.out.println(userDto);
        userValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.register(userDto);
        redirectAttributes.addAttribute("userAddedSuccessfully", true);
        return "redirect:/register";

    }

    @GetMapping("/login")
    public String loginGet(){
        return "login";
    }
}